import firebase from 'firebase';
import 'firebase/firestore'

const firebaseConfig2 = {
    apiKey: "AIzaSyBZ6oVwSNvNrrz4ymwIlzbQlhJ42vwyL8c",
    authDomain: "project9-dam.firebaseapp.com",
    databaseURL: "https://project9-dam.firebaseio.com",
    projectId: "project9-dam",
    storageBucket: "project9-dam.appspot.com",
    messagingSenderId: "793226959013",
    appId: "1:793226959013:web:85ce638f11be3f078fd54c"
};

const config = {
  name: 'SECONDARY_APP',
};

// Initialize firebase
firebase.initializeApp(firebaseConfig2, config);

const otherApp = firebase.app('SECONDARY_APP');
export const db2 = firebase.firestore(otherApp)

db2.settings({
    experimentalForceLongPolling: true
  })




// export default firebase;