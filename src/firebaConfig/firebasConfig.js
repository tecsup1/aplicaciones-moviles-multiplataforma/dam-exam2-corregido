import firebase from 'firebase'
import 'firebase/firestore'
import 'firebase/auth'


const firebaseConfig = {
  apiKey: "AIzaSyCuG1E_-L0jtVJZ9AS2V3T_FZFLhENH1p0",
  authDomain: "fir-redux-b8cb8.firebaseapp.com",
  databaseURL: "https://fir-redux-b8cb8.firebaseio.com",
  projectId: "fir-redux-b8cb8",
  storageBucket: "fir-redux-b8cb8.appspot.com",
  messagingSenderId: "78772459317",
  appId: "1:78772459317:web:f3ce2107197e2680181e7c"
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export const auth = firebase.auth()
export const db = firebase.firestore()


db.settings({
  experimentalForceLongPolling: true
})