import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import {db} from '../../firebaConfig/firebasConfig';
import {setPedidoAction} from '../../reduxDucks/restaurantDucks';
import {connect} from 'react-redux';
import moment from 'moment';
moment.locale('es');

class HacerPedidoView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cantidad: 1,
    };
  }

  async componentDidMount() {
    // const { setPedidoAction } = this.props
    // setPedidoAction()
    // console.log("restaurantes papurro: "+this.props.restaurants)
  }

  render() {
    const item = this.props.route.params.item;
    const precioTotal = Math.round(parseFloat(item.price) * this.state.cantidad *100)/100;
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 0.98,
            backgroundColor: '#FF8E01',
            marginHorizontal: 15,
            marginTop: 15,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
          }}>
          <Text
            style={{
              flex: 0.1,
              fontSize: 35,
              textAlign: 'center',
              margin: 5,
              color: 'white',
            }}>
            Producto: {item.combo_name}
          </Text>
          <Text
            style={{
              flex: 0.3,
              marginHorizontal: 20,
              fontSize: 23,
              color: 'white',
            }}>
            Incluye: {'\n'}
            {item.description}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                marginHorizontal: 20,
                fontSize: 25,
                color: 'white',
              }}>
              Cantidad deseada:
            </Text>
            <TextInput
              style={{
                // flex: 0.02,
                height: 40,
                width: 150,
                borderColor: 'gray',
                borderWidth: 1,
                backgroundColor: 'white',
              }}
              onChangeText={(text) => this.setState({cantidad: text})}
              value={this.state.cantidad}
              placeholder="Ingrese la cantidad"
            />
          </View>

          <Text
            style={{
              flex: 0.1,
              marginHorizontal: 20,
              fontSize: 30,
              color: 'white',
            }}>
            Costo total: S/. {precioTotal}
          </Text>

          <TouchableOpacity
            style={{
              flex: 0.05,
              backgroundColor: '#568B2F',
              marginBottom: 15,
              alignSelf: 'center',
              width: '40%',
              borderRadius: 15,
              padding: 10,
              shadowColor: '#000',
              shadowOffset: {
                width: 4,
                height: 4,
              },

              shadowOpacity: 0.5,
              shadowRadius: 4.65,
              elevation: 4,
            }}
            onPress={() => {
              const fecha = moment(new Date()).format('DD/MM/YYYY');

              this.props.setPedidoAction({
                cantidad: this.state.cantidad,
                nombre_pedido: item.combo_name,
                fecha: fecha,
                precio: precioTotal,
              });

              Alert.alert('Pedido confirmado');
              this.props.navigation.navigate('Reservas', {});
            }}>
            <Text style={{textAlign: 'center', color: 'white', fontSize: 20}}>
              Confirmar pedido
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
// export default HacerPedidoView;

const mapStateToProps = (store) => ({
  pedidosArray: store.restaurantDucks.pedidosArray,
});

const mapDispatchToProps = {
  setPedidoAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(HacerPedidoView);
