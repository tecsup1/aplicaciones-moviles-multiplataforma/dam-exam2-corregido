import React, { useEffect } from "react";
import {
  View,
  Text,
  Button,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Callout, Marker } from "react-native-maps";


const { width, height } = Dimensions.get("window");

export default ({ navigation, route }) => {
  const { nombre: restaurante } = route.params;
  useEffect(() => {
    navigation.setOptions({ title: route.params.nombre });
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.imagencontainer}>
          <Image
            source={{
              uri: `${route.params.imagen}`,
            }}
            style={{
              width: width,
              height: height / 3,
              flex: 1,
            }}
          />
        </View>
        <View style={styles.container2}>
          <Text>{route.params.nombre}</Text>
          <Text>Direccion: {route.params.direccion}</Text>
          <Text>Estado: {route.params.estado}</Text>
          <Text>Telefono: {route.params.telefono}</Text>
          <Text>Rating: {route.params.rating}</Text>
        </View>

        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#FB5757" }]}
          onPress={() => navigation.navigate("First", {restaurante: route.params.nombre})}
        >
          <Text style={{ textAlign: "center", color: "white" }}>Reservar</Text>
        </TouchableOpacity>

        <View style={{ flex: 1, height: 300 }}>
          <Text>Donde ubicarnos?</Text>
          <MapView
            style={{ height: 290 }}
            provider={PROVIDER_GOOGLE}
            initialRegion={{
              latitude: route.params.latitud,
              longitude: route.params.longitud,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            <Marker
              coordinate={{
                latitude: route.params.latitud,
                longitude: route.params.longitud,
              }}
            >
              <Callout>
                <View
                  style={{
                    borderRadius: 5,
                  }}
                >
                  <Text>{route.params.nombre}</Text>
                  <Image
                    source={{
                      uri: `${route.params.imagen}`,
                    }}
                    style={{ width: 77, height: 77 }}
                  />
                </View>
              </Callout>
            </Marker>
          </MapView>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imagencontainer: {
    width: width,
    height: height / 3,
  },
  container2: {
    width: width,
    height: 190,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "white",
    marginTop: 20,
    padding: 15,
  },
  button: {
    margin: 15,
    height: 30,
    justifyContent: "center",
    width: 90,
    backgroundColor: "#428AF8",
    borderRadius: 10,
  },
});
