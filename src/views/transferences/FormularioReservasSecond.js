import moment from "moment";
moment.locale("es");

import React, { useState } from "react";
import { useDispatch } from 'react-redux'
import { Text, View, Button, TouchableOpacity } from "react-native";
import { setReservas } from '../../reduxDucks/reducer';
export default function TransferenceSecond({ navigation, route }) {
  // const [importe, setImporte] = useState(0)
  const dispatch = useDispatch()
  console.log(route.params)
  const { nombre, email, celular, fecha, tiempo, personas } = route.params;

  const data = route.params
  const sendReserva = () => {
    dispatch(setReservas(data))
    navigation.navigate("Third")
  }

  return (
    <View
      style={{
        flex: 1,
        padding: 28,
      }}
    >
      {/* <Text>{JSON.stringify(route.params)}</Text> */}
      <View>
        <Text>Nombre</Text>
        <Text
          style={{
            borderBottomColor: "gray",
            borderBottomWidth: 3,
            marginBottom: 6,
          }}
        >
          {nombre}
        </Text>
      </View>
      <View>
        <Text>Email</Text>
        <Text
          style={{
            borderBottomColor: "gray",
            borderBottomWidth: 3,
            marginBottom: 6,
          }}
        >
          {email}
        </Text>
      </View>
      <View>
        <Text>Celular</Text>
        <Text
          style={{
            borderBottomColor: "gray",
            borderBottomWidth: 3,
            marginBottom: 6,
          }}
        >
          {celular}
        </Text>
      </View>
      <View>
        <Text>Reservada para la fecha:</Text>
        <Text
          style={{
            borderBottomColor: "gray",
            borderBottomWidth: 3,
            marginBottom: 6,
          }}
        >
          {fecha}
        </Text>
      </View>
      <View>
        <Text>Hora de reservación:</Text>
        <Text
          style={{
            borderBottomColor: "gray",
            borderBottomWidth: 3,
            marginBottom: 6,
          }}
        >
          {tiempo}
        </Text>
      </View>
      <View>
        <Text>Reserva para: </Text>
        <Text
          style={{
            borderBottomColor: "gray",
            borderBottomWidth: 3,
            marginBottom: 6,
          }}
        >
          {personas} personas
        </Text>
      </View>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          marginTop: 10,
        }}
      >
        <TouchableOpacity
          style={{
            alignItems: "center",
            backgroundColor: "#808080",
            padding: 10,
            width: 120,
            borderRadius: 10,
            marginBottom: 10,
            marginRight: 20,
            // opacity: 0.5
          }}
          onPress={() => navigation.navigate("First")}
        >
          <Text style={{ color: "white" }}>VOLVER</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            alignItems: "center",
            backgroundColor: "#063984",
            padding: 10,
            width: 120,
            borderRadius: 10,
            marginBottom: 10,
            // opacity: 0.5
          }}
          onPress={sendReserva}
        >
          <Text style={{ color: "white" }}>CONFIRMAR</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
