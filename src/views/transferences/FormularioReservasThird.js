import moment from 'moment'
moment.locale("es")

import React, { useState } from 'react'
import { CommonActions } from '@react-navigation/native';
import {
  Text,
  View,
  Button,
  Image,
  TouchableOpacity
} from 'react-native'

import source from '../../assets/header/header.png'

export default function TransferenceThrid({ navigation }) {



  return (
    <View style={{
      flex: 1,
    }}>
      <View style={{
        width: 200,
        flex: 1,
        alignSelf: "center",
        justifyContent: "center"
      }}>
        <Image source={source}
          style={{
            width: 150,
            height: 150,
            alignSelf: "center",
            marginBottom: 20
          }}
        />
        <TouchableOpacity
          style={{
            alignItems: "center",
            backgroundColor: "#063984",
            padding: 10,
            width: 200,
            borderRadius: 10,
            marginBottom: 10,
            alignSelf: "center"
            // opacity: 0.5
          }}
          onPress={() => navigation.navigate("Reservas")}

        >
          <Text style={{ color: "white" }}>Ver Reservas</Text>

        </TouchableOpacity>
      </View>

    </View>
  )
}