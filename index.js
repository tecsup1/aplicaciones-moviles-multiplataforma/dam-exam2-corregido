/**
 * @format
 */
import React from 'react'
import { Provider } from "react-redux";
import {AppRegistry} from 'react-native';
import store from './src/reduxDucks/store';
import App from './src/views/MainStackContainer';
import {name as appName} from './app.json';
import { YellowBox } from 'react-native';

const RealApp = () => {


  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}



YellowBox.ignoreWarnings(['Setting a timer']);
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => RealApp);
